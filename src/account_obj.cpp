#include <QDebug>
#include "account_obj.h"

AccountObj::AccountObj(QObject *parent)
    : QObject(parent)
{
}

AccountObj::AccountObj(const int &id, const QString &name, const QString &secret, QObject *parent)
    : QObject(parent), m_id(id), m_name(name), m_secret(secret), m_token(""), m_deleteVis(false), m_copyVis(true), m_visible(true)
{
}
