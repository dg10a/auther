#pragma once 

#include <QObject>

class AccountObj : public QObject
{
    Q_OBJECT

    Q_PROPERTY(int id MEMBER m_id NOTIFY idChanged)
    Q_PROPERTY(QString name MEMBER m_name NOTIFY nameChanged)
    Q_PROPERTY(QString token MEMBER m_token NOTIFY tokenChanged)
    Q_PROPERTY(bool deleteVis MEMBER m_deleteVis NOTIFY deleteVisChanged)
    Q_PROPERTY(bool copyVis MEMBER m_copyVis NOTIFY copyVisChanged)
    Q_PROPERTY(bool visible MEMBER m_visible NOTIFY visibleChanged)

public:
    AccountObj(QObject *parent=0);
    AccountObj(const int &id, const QString &name, const QString &secret, QObject *parent=0);

    int m_id;
    QString m_name;
    QString m_token;
    QString m_secret;
    bool m_deleteVis;
    bool m_copyVis;
    bool m_visible;

signals:
    void idChanged();
    void nameChanged();
    void tokenChanged();
    void deleteVisChanged();
    void copyVisChanged();
    void visibleChanged();

private:
};
