import QtQuick 2.6
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.0
import QtQuick.Controls.Material 2.12

import auther.backend 0.0

ApplicationWindow {
    Material.theme: MPalette.theme
    Material.background: MPalette.background

    id: root
    width: 350
    height: 500
    flags: Qt.Dialog
    visible: true

    BackEnd {
        id: backend
    }

    Popup {
        id: addPopup
        modal: true
        focus: true
        closePolicy: Popup.NoAutoClose
        x: (parent.width - width) / 2
        y: (parent.height - height) / 2

        ColumnLayout {
            anchors.fill: parent

            TextField {
                id: addNameTextField
                placeholderText: qsTr("Name")
            }
            TextField {
                id: addSecretTextField
                placeholderText: qsTr("Secret")
            }
            RowLayout {
                Button {
                    id: addDoneButton
                    text: qsTr("Done")
                    onClicked: {
                        backend.addAccount(addNameTextField.text, addSecretTextField.text)
                        addNameTextField.text = ""
                        addSecretTextField.text = ""
                        addPopup.close()
                    }
                }
                Button {
                    id: addCancelButton
                    text: qsTr("Cancel")
                    onClicked: {
                        addNameTextField.text = ""
                        addSecretTextField.text = ""
                        addPopup.close()
                    }
                }
            }
        }
    }

    ColumnLayout {
        anchors.fill: parent

        RowLayout {
	    Item {
		Layout.fillWidth: true
	    }

            Button {
                id: addButton
                text: qsTr("Add")
                visible: backend.mode == 0
                onClicked:
                {
                    addPopup.open()
                }
            }

            Button {
                id: deleteButton
                text: qsTr("Delete")
                visible: backend.mode == 0
                onClicked:
                {
                    backend.setDeleteMode()
                }
            }

            Button {
                id: searchButton
                visible: backend.mode == 0
                text: qsTr("Search")
                onClicked:
                {
                    backend.setSearchMode()
                }
            }

            TextField {
                id: searchTextField
                placeholderText: qsTr("Search")
                visible: backend.mode == 2
                onTextEdited: {
                    backend.searchTextUpdated(this.text)
                }
            }

            Button {
                id: doneButton
                text: qsTr("Done")
                visible: backend.mode == 1 || backend.mode == 2
                onClicked:
                {
                    searchTextField.text = ""
                    backend.setMainMode()
                }
            }

	    Item {
		Layout.fillWidth: true
	    }
        }
        ListView {
            id: accountsListView
            flickableDirection: Flickable.VerticalFlick
            boundsBehavior: Flickable.StopAtBounds
            clip: true
            Layout.fillWidth: true
            Layout.fillHeight: true
            ScrollBar.vertical: ScrollBar {}
            spacing: 0
            model: backend.model

            delegate: RowLayout {
                height: this.visible * childrenRect.height
                anchors.left: parent.left
                anchors.right: parent.right
                Layout.alignment: Qt.AlignCenter
                visible: modelData.visible

                Item {
                    Layout.fillWidth: true
                }
                ColumnLayout {
                    Layout.alignment: Qt.AlignCenter
                    spacing: 0

                    Text {
                        text: modelData.token
                        font.pixelSize: 0.035 * Screen.height
                    }
                    Text {
                        text: modelData.name
                        font.pixelSize: 0.02 * Screen.height
                    }
                }
                Item {
                    Layout.fillWidth: true
                }
                Button {
                    id: copyButton
                    text: qsTr("Copy")
                    visible: modelData.copyVis
                    onClicked: {
                        backend.copyToken(modelData.token)
                    }
                }
                Button {
                    id: accountDeleteButton
                    text: qsTr("Delete")
                    visible: modelData.deleteVis
                    onClicked: {
                        backend.deleteAccount(modelData.id)
                    }
                }
                Item {
                    Layout.fillWidth: true
                }
            }
        }
    }
}
