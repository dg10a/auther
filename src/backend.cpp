#include <QDebug>
#include <QSqlDriver>
#include <QSqlError>
#include <QSqlQuery>
#include <QTimer>
#include <QApplication>
#include <QClipboard>

#include "backend.h"
#include "keepassxc/totp/totp.h"

BackEnd::BackEnd(QObject *parent) :
    QObject(parent)
{
    // get data directory path and create if it doesn't exist
    dataDir = QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation));
    dataDir.mkpath(dataDir.absolutePath());

    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(dataDir.absolutePath() + QDir::separator() + "data.sqlite");

    loadAccounts();

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &BackEnd::refreshAccounts);
    timer->start(100);
}

bool BackEnd::checkSecret(const QString &secret)
{
    if (Totp::generateTotp(secret.toLatin1(), std::time(0), 6, 30) == "Invalid TOTP secret key")
        return false;
    else
        return true;
}

void BackEnd::refreshAccounts(void)
{
    foreach (QObject* item, m_model)
    {
        AccountObj *obj = qobject_cast<AccountObj *>(item);
        obj->m_token = getTOTP(obj->m_secret);
        obj->tokenChanged();
    }
}

void BackEnd::copyToken(const QString &token)
{
    QApplication::clipboard()->setText(token);
}

void BackEnd::loadAccounts(void)
{
    if(!db.open()) {
        qWarning() << "ERROR: " << db.lastError();
    } else {
        // clear old accounts
        m_model.clear();

        QSqlQuery query("CREATE TABLE IF NOT EXISTS Accounts(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Secret TEXT)");

        query.prepare("SELECT * FROM Accounts");
        query.exec();

        while (query.next()) {
            int id = query.value(0).toInt();
            QString name(query.value(1).toString());
            QString secret(query.value(2).toString());
            m_model.prepend(new AccountObj(id, name, secret));
        }
        db.close();
        modelChanged();
    }
}

void BackEnd::deleteAccount(const int &id)
{
    if(!db.open()) {
        qWarning() << "ERROR: " << db.lastError();
    } else {
        QSqlQuery query("CREATE TABLE IF NOT EXISTS Accounts(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Secret TEXT)");
        query.prepare("DELETE FROM Accounts WHERE Id=:id");
        query.bindValue(":id", id);
        query.exec();
        db.close();

        for (int i = 0; i < m_model.count(); ++i)
        {
            AccountObj *obj = qobject_cast<AccountObj *>(m_model[i]);
            if (obj->m_id == id) {
                m_model.removeAt(i);
                break;
            }
        }
        modelChanged();
    }
}

void BackEnd::setMainMode(void)
{
    foreach (QObject* item, m_model)
    {
        AccountObj *obj = qobject_cast<AccountObj *>(item);
        obj->m_copyVis = true;
        obj->m_deleteVis = false;
        obj->deleteVisChanged();
        obj->copyVisChanged();
        if (obj->m_visible == false) {
            obj->m_visible = true;
            obj->visibleChanged();
        }
    }
    m_mode = 0;
    modeChanged();
}

void BackEnd::searchTextUpdated(const QString &text)
{
    if (m_mode == 2) {
        foreach (QObject* item, m_model)
        {
            AccountObj *obj = qobject_cast<AccountObj *>(item);
            if (obj->m_name.toLower().contains(text.toLower())) {
                if (obj->m_visible == false) {
                    obj->m_visible = true;
                    obj->visibleChanged();
                }
            } else {
                if (obj->m_visible == true) {
                    obj->m_visible = false;
                    obj->visibleChanged();
                }
            }
        }
    }
}

void BackEnd::setDeleteMode(void)
{
    foreach (QObject* item, m_model)
    {
        AccountObj *obj = qobject_cast<AccountObj *>(item);
        obj->m_deleteVis = true;
        obj->m_copyVis = false;
        obj->copyVisChanged();
        obj->deleteVisChanged();
    }
    m_mode = 1;
    modeChanged();
}

void BackEnd::setSearchMode(void)
{
    m_mode = 2;
    modeChanged();
}

QString BackEnd::getTOTP(const QString &secret)
{
    return Totp::generateTotp(secret.toLatin1(), std::time(0), 6, 30);
}

void BackEnd::addAccount(const QString &name, const QString &secret)
{
    if(!db.open()) {
        qWarning() << "ERROR: " << db.lastError();
    } else {
        if (checkSecret(secret)) {
            QSqlQuery query("CREATE TABLE IF NOT EXISTS Accounts(Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Secret TEXT)");
            query.prepare("INSERT INTO Accounts VALUES(NULL, :name, :secret)");
            query.bindValue(":name", name);
            query.bindValue(":secret", secret);

            query.exec();
            db.close();
            loadAccounts();
        } else {
            qWarning() << "Invalid secret";
        }
    }
}
