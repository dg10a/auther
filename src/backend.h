#pragma once

#include <QObject>
#include <QString>
#include <QSqlDatabase>
#include <QStandardPaths>
#include <QDir>

#include "account_obj.h"

class BackEnd : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QList<QObject*> model MEMBER m_model NOTIFY modelChanged)
    Q_PROPERTY(int mode MEMBER m_mode NOTIFY modeChanged)

public:
    explicit BackEnd(QObject *parent = nullptr);
    Q_INVOKABLE void addAccount(const QString &name, const QString &secret);
    Q_INVOKABLE void loadAccounts(void);
    Q_INVOKABLE void refreshAccounts(void);
    Q_INVOKABLE void copyToken(const QString &token);
    Q_INVOKABLE void setDeleteMode(void);
    Q_INVOKABLE void setSearchMode(void);
    Q_INVOKABLE void searchTextUpdated(const QString &text);
    Q_INVOKABLE void setMainMode(void);
    Q_INVOKABLE void deleteAccount(const int &id);

    QList<QObject*> m_model;
    int m_mode = 0;

signals:
    void modelChanged();
    void modeChanged();

private:
    bool checkSecret(const QString &secret);
    QString getTOTP(const QString &secret);

    QDir dataDir;
    QSqlDatabase db;
};
